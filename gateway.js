const WebSocket = require('ws');

class Gateway {
    constructor(){
    }

    #onClose;
    #heartbeat = null;
    #seq = null;
    #errorCb;
    #callbacks = {};

    #sendPacket(packet){
        this.socket.send(JSON.stringify(packet));
    }

    #onMessage(message){
        let packet = JSON.parse(message);

        if(packet.s !== null) this.#seq = packet.s;

        switch(packet.op){
            case 0:
                if(packet.t === 'READY'){
                    this.session_id = packet.d.session_id;
                    this.user_id = packet.d.user.id;
                }
                if(typeof this.#callbacks[packet.t] === 'function') this.#callbacks[packet.t](packet.d);
                break;
            case 7:
                this.#sendPacket({
                    op: 6,
                    d: {
                      token: this.identity.token,
                      session_id: this.session_id,
                      seq: this.#seq
                    }
                  });
                break;
            case 9:
                this.#sendPacket({
                    op: 2,
                    d: this.identity
                });
                break;
            case 10:
                if(this.#heartbeat != null) clearInterval(this.#heartbeat);
                const that = this;
                this.#heartbeat = setInterval(()=>{
                    that.#sendPacket({
                        op: 1,
                        d: that.#seq
                    });
                }, packet.d.heartbeat_interval);
                this.#sendPacket({
                    op: 2,
                    d: this.identity
                });
                break;
            // we can ignore received Heartbeats
            case 1:
            // send only opcodes
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 8:
            // we can ignore Heartbeat ACK
            case 11:
            default:
                break;
        }
    }

    #events = [
        'READY',
        'READY_SUPPLEMENTAL',
        'RESUMED',
        'GUILD_CREATE',
        'GUILD_DELETE',
        'GUILD_UPDATE',
        'INVITE_CREATE',
        'INVITE_DELETE',
        'GUILD_MEMBER_ADD',
        'GUILD_MEMBER_REMOVE',
        'GUILD_MEMBER_UPDATE',
        'GUILD_MEMBERS_CHUNK',
        'GUILD_ROLE_CREATE',
        'GUILD_ROLE_DELETE',
        'GUILD_ROLE_UPDATE',
        'GUILD_BAN_ADD',
        'GUILD_BAN_REMOVE',
        'GUILD_EMOJIS_UPDATE',
        'GUILD_INTEGRATIONS_UPDATE',
        'CHANNEL_CREATE',
        'CHANNEL_DELETE',
        'CHANNEL_UPDATE',
        'CHANNEL_PINS_UPDATE',
        'MESSAGE_CREATE',
        'MESSAGE_DELETE',
        'MESSAGE_UPDATE',
        'MESSAGE_DELETE_BULK',
        'MESSAGE_REACTION_ADD',
        'MESSAGE_REACTION_REMOVE',
        'MESSAGE_REACTION_REMOVE_ALL',
        'MESSAGE_REACTION_REMOVE_EMOJI',
        'USER_UPDATE',
        'PRESENCE_UPDATE',
        'TYPING_START',
        'VOICE_STATE_UPDATE',
        'VOICE_SERVER_UPDATE',
        'WEBHOOKS_UPDATE'
    ]

    identify(identity){
        this.socket = new WebSocket('wss://gateway.discord.gg/?encoding=json&v=8');
        this.identity = identity;
        this.socket.on('message', (message)=>{
            this.#onMessage(message);
        });
        const that = this;
        this.socket.on('close', function(code, reason){
            if(that.#heartbeat != null) clearInterval(that.#heartbeat);
            if(that.#onClose) that.#onClose(code, reason.toString('utf-8'));
        });
        this.socket.on('error', (error)=>{
            if(typeof that.#errorCb == 'function') that.#errorCb(error);
        });
    }

    onEvent(event, callback){
        if(typeof callback !== 'function') throw 'Callback is not a function';
        if(!this.#events.includes(event)) return;
        this.#callbacks[event] = callback;
    }

    onError(callback){
        this.#errorCb = callback;
    }

    close(){
        this.socket.close();
        if(this.#heartbeat != null) clearInterval(this.#heartbeat);
    }

    onClose(cb){
        this.#onClose = cb;
    }
}

module.exports = Gateway;
