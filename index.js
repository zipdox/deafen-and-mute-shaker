/*
    Deafen and Mute Shaker
    Copyright (C) 2023  Zipdox

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const Gateway = require('./gateway.js');
const fetch = require('node-fetch');
const {token, guilds} = require('./config.json');

const states = {};
guilds.forEach(guild=>{
    if(typeof guild.id != 'string'){
        console.error('Invalid guild ID in config');
        process.exit();
    }
    if(typeof guild.channel_id != 'string'){
        console.error('Invalid guild channel ID in config');
        process.exit();
    }
    if(typeof guild.timeout != 'number'){
        console.error('Invalid guild timeout in config');
        process.exit();
    }
    states[guild.id] = {};
});

const gateway = new Gateway();

function identify(){
    gateway.identify({
        token,
        intents: 1 << 7,
        properties: {
            os: 'Linux',
            browser: 'Firefox'
        }
    });
}
identify();

gateway.onEvent('READY', (msg)=>{
    console.log('Logged in as', msg.user.username);
});

gateway.onClose((code, reason)=>{
    console.log('Gateway closed', code, reason);
    identify();
});

gateway.onError((error)=>{
    console.error(error);
});

function move(guild_id, user_id, channel_id){
    fetch(`https://discord.com/api/v9/guilds/${guild_id}/members/${user_id}`, {
        headers: {
            "Authorization": 'Bot ' + token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({channel_id}),
        method: "PATCH"
    });
}

function disconnect(guild_id, user_id){
    fetch(`https://discord.com/api/v9/guilds/${guild_id}/members/${user_id}`, {
        headers: {
            "Authorization": 'Bot ' + token,
            "Content-Type": "application/json",
        },
        body: "{\"channel_id\":null}",
        method: "PATCH"
    });
}

function deafen(guildConfig, user_id, channel_id){
    const guildState = states[guildConfig.id];
    if(guildState[user_id]?.timeout) clearTimeout(guildState[user_id].timeout);
    guildState[user_id] = {channel_id};
    guildState[user_id].timeout = setTimeout(
        function(){
            move(guildConfig.id, user_id, guildConfig.channel_id);
            delete guildState[user_id].timeout;
        },
        guildConfig.timeout
    );
}

function undeafen(guildConfig, user_id, channel_id){
    const guildState = states[guildConfig.id];
    if(!guildState[user_id]){
        if(channel_id == guildConfig.channel_id) disconnect(guildConfig.id, user_id);
        return;
    }
    if(guildState[user_id].timeout) clearTimeout(guildState[user_id].timeout);
    if(channel_id == guildConfig.channel_id) move(guildConfig.id, user_id, guildState[user_id].channel_id);
    delete guildState[user_id];
}

function userDisconnect(guild_id, user_id){
    const guildState = states[guild_id];
    if(!guildState[user_id]) return;
    if(guildState[user_id].timeout) clearTimeout(guildState[user_id].timeout);
    delete guildState[user_id];
}

function voiceStateUpdate(update){
    const guild_id = update.guild_id;
    if(!guild_id) return;

    const guildConfig = guilds.find(guild=>guild.id == guild_id);
    if(!guildConfig) return;
    if(guildConfig.bypass_role_id) if(update.member.roles.includes(guildConfig.bypass_role_id)) return;

    if(update.channel_id == null) return userDisconnect(guild_id, update.user_id);

    if(update.self_deaf){
        if(update.channel_id == guildConfig.channel_id) return;
        deafen(guildConfig, update.user_id, update.channel_id);
    }else{
        undeafen(guildConfig, update.user_id, update.channel_id);
    }
    
}

gateway.onEvent('VOICE_STATE_UPDATE', voiceStateUpdate);
