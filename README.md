# Deafen and Mute Shaker

Discord bot that moves users to a specific channel after a while if they deafen and mute.

## Configuration

Create `config.json` with the following format:

```json
{
    "token": "DISCORD_BOT_TOKEN",
    "guilds": [
        {
            "id": "GUILD_ID",
            "channel_id": "DEAFENED_CHANNEL_ID",
            "timeout": 10000
        }
    ]
}
```

`guilds` is an array of objects for each guild you want the bot to work in. `timeout` is in milliseconds.

## License

This code is licensed under GPL-3.0.
